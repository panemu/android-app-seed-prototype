package com.panemu.androidappseed;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.panemu.androidappseed.api.ApiCity;
import com.panemu.androidappseed.api.RetrofitClientInstance;
import com.panemu.androidappseed.data.City;
import com.panemu.androidappseed.data.DtoCountry;
import com.panemu.androidappseed.misc.TableCriteria;
import com.panemu.androidappseed.misc.TableQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CityFormSearcFragment extends Fragment {

    private ProgressDialog progress;
    private RecyclerView recyclerView;
    Spinner continenSpinner, countrySpinner;
    EditText cityName;
    private Map<String,Integer> mapCountry = new HashMap<>();

    public CityFormSearcFragment() {
        // Required empty public constructor
    }

    public static CityFormSearcFragment newInstance(String param1, String param2) {
        CityFormSearcFragment fragment = new CityFormSearcFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_city_form_searc, container, false);
        final EditText cityName = (EditText) view.findViewById(R.id.etsCityName);

        continenSpinner = view.findViewById(R.id.spnContinen);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(), R.array.spinner_continent, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        continenSpinner.setAdapter(adapter);
//        String emptiValue = "";
        continenSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                spinnerCountry();
                Object item = parent.getItemAtPosition(position);
                continenSpinner.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        countrySpinner = view.findViewById(R.id.spnCountry);
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                Object itemCountry = parent.getItemAtPosition(position);
                countrySpinner.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        Button btnSearchCity = view.findViewById(R.id.btnSearchCity);
        btnSearchCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = (String) cityName.getText().toString();
                TableQuery tq = new TableQuery();
                if (name != null && !name.trim().isEmpty()) {
                    TableCriteria tc = new TableCriteria();
                    tc.attributeName = "name";
                    tc.value = name.trim();
                    tq.tableCriteria.add(tc);
                }

                String country = countrySpinner.getSelectedItem().toString();
                Integer countryId = mapCountry.get(country);
                if (country != null && !country.trim().isEmpty()) {
                    TableCriteria tc = new TableCriteria();
                    tc.attributeName = "country";
                    tc.value = countryId.toString();
                    tq.tableCriteria.add(tc);
                }

                StaticObject.searchData = tq;
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.popBackStack();
            }
        });

        return view;
    }

    private void showDetail(City item) {
        Log.d("panemu", "show detail from parent fragment");
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        navController.popBackStack();
    }

    private void spinnerCountry(){
        String textContinen = continenSpinner.getSelectedItem().toString();
        ApiCity api = RetrofitClientInstance.getRetrofitInstance().create(ApiCity.class);
        Call<List<DtoCountry>> call = api.getCountryByContinent(textContinen);
        call.enqueue(new Callback<List<DtoCountry>>() {
            @Override
            public void onResponse(Call<List<DtoCountry>> call, Response<List<DtoCountry>> response) {
                List<DtoCountry> lstCountry = response.body();
                List<String> lstSpinner = new ArrayList<String>();
                mapCountry.clear();
                for (int i = 0; i < lstCountry.size(); i++){
                    lstSpinner.add(lstCountry.get(i).name);
                    mapCountry.put(lstCountry.get(i).name, lstCountry.get(i).id);
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(CityFormSearcFragment.this.getContext(), android.R.layout.simple_spinner_item, lstSpinner);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                countrySpinner.setAdapter(adapter);
                countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                        Object item = parent.getItemAtPosition(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<DtoCountry>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
