package com.panemu.androidappseed.data;

import java.io.Serializable;
import java.math.BigInteger;

public class DtoCountry implements Serializable {
    public Integer id;
    public String name;
    public String capital;
    public String continent;
    public String independence;
    public BigInteger population;
    public Integer version;
}
