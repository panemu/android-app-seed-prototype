package com.panemu.androidappseed.data;

import java.io.Serializable;

public class City implements Serializable {
    public String continent;
    public Integer countryId;
    public String countryName;
    public Integer id;
    public String name;
    public Integer version;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
