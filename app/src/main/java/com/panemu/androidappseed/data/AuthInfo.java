package com.panemu.androidappseed.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AuthInfo  implements Serializable {
    public String authToken;
    public Integer id;
    public List<String> permissions = new ArrayList<>();
    public String role;
    public String username;
}
