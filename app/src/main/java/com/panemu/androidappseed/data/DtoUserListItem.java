package com.panemu.androidappseed.data;

import java.io.Serializable;
import java.util.Date;

public class DtoUserListItem implements Serializable {
    public String email;
    public Integer id;
    public long modifiedDate;
    public String role;
    public String username;
    public Integer version;
}
