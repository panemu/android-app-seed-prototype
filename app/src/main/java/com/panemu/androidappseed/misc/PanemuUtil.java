package com.panemu.androidappseed.misc;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Message;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import com.panemu.androidappseed.api.RetrofitClientInstance;
import com.panemu.androidappseed.data.AuthInfo;
import com.panemu.androidappseed.misc.function.Consumer;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class PanemuUtil {

    public enum MessageType {INFO, ERROR, CONFIRMATION}

    public enum MessageAnswer {YES_OK, NO, CANCEL}

    private static AuthInfo authInfo;

    public static RestError parseError(Response<?> response) {
        Converter<ResponseBody, RestError> converter =
                RetrofitClientInstance.getRetrofitInstance()
                        .responseBodyConverter(RestError.class, new Annotation[0]);

        RestError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new RestError();
        }

        return error;
    }

    /**
     *
     * @param c
     * @param type nullable
     * @param title nullable
     * @param message
     * @param yesOkLabel nullable
     * @param noLabel nullable
     * @param cancelLabel nullable
     * @param _callback nullable
     */
    public static void showMessageDialog(Context c,
                                                  MessageType type,
                                                  String title,
                                                  String message,
                                                  String yesOkLabel,
                                                  String noLabel,
                                                  String cancelLabel,
                                                  Consumer<MessageAnswer> _callback) {
        final Consumer<MessageAnswer> fcallback = _callback == null ? m ->{} : _callback;


        AlertDialog.Builder builder = new AlertDialog.Builder(c);

        builder.setMessage(message);
        if (yesOkLabel == null || yesOkLabel.trim().isEmpty()) {
            yesOkLabel = "OK";
        }
        builder.setPositiveButton(yesOkLabel, (dialog, which) -> fcallback.accept(MessageAnswer.YES_OK));
        if (type == null) type = MessageType.INFO;

        if (title == null || title.trim().isEmpty()) {
            switch (type) {
                case INFO:
                    title = "Information";
                    break;
                case ERROR:
                    title = "Error";
                    break;
                case CONFIRMATION:
                    title = "Confirmation";
                    break;
            }
        }
        builder.setTitle(title);

        if (noLabel != null && !noLabel.trim().isEmpty()) {
            builder.setNegativeButton(noLabel, (dialog, which) -> fcallback.accept(MessageAnswer.NO));
        }

        if (cancelLabel != null && !cancelLabel.trim().isEmpty()) {
            builder.setNeutralButton(cancelLabel, (dialog, which) -> fcallback.accept(MessageAnswer.CANCEL));
        }
        builder.show();
    }

    public static void showDialogInfo(Context c, String message) {
        showDialogInfo(c, message, null);
    }

    public static void showDialogInfo(Context c, String message, Consumer<MessageAnswer> _callback) {
        showMessageDialog(c, MessageType.INFO, null, message, null, null, null, _callback);
    }

    public static void showDialogError(Context c, String message) {
        showDialogError(c, message, null);
    }

    /**
     *
     * @param c
     * @param message
     * @param _callback nullable
     */
    public static void showDialogError(Context c, String message, Consumer<MessageAnswer> _callback) {
        showMessageDialog(c, MessageType.ERROR, null, message, null, null, null, _callback);
    }

    public static void showDialogConfirmation(Context c, String message, boolean showCancelButton, Consumer<MessageAnswer> _callback) {
        String cancelLabel = showCancelButton ? "Cancel" : null;
        showMessageDialog(c, MessageType.CONFIRMATION, null, message, "Yes", "No", cancelLabel, _callback);
    }

    public static void setAuthInfo(AuthInfo authInfo) {
        PanemuUtil.authInfo = authInfo;
    }

    public static AuthInfo getAuthInfo() {
        return authInfo;
    }

    public static ProgressDialog showProgressDialog(Context context) {
        ProgressDialog progress = new ProgressDialog(context);
        progress.setCancelable(false);
        progress.setMessage("Wait a second ...");
        progress.setCancelable(true);
        progress.show();
        return progress;
    }

}
