package com.panemu.androidappseed.misc;

import java.util.ArrayList;
import java.util.List;

public class TableData<T> {
    public List<T> rows = new ArrayList<>();
    public int totalRows;
}
