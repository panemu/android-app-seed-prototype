package com.panemu.androidappseed.misc.function;

public interface Consumer<T> {

    void accept(T t);

}
