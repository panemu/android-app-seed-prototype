package com.panemu.androidappseed.misc;

import androidx.annotation.NonNull;

public class RestError {

    public String code;
    public String message;

    @Override
    public String toString() {
        return "RestError{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
