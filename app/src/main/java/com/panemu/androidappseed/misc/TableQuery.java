package com.panemu.androidappseed.misc;

import java.util.ArrayList;
import java.util.List;

public class TableQuery {
    public List<TableCriteria> tableCriteria = new ArrayList<>();
    public List<SortingInfo> sortingInfos = new ArrayList<>();
    public int utcMinuteOffset;
}
