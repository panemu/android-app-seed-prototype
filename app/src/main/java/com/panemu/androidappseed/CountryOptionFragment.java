package com.panemu.androidappseed;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;


public class CountryOptionFragment extends BottomSheetFragment {

//    private OnFragmentInteractionListener mListener;

    public CountryOptionFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static CountryOptionFragment newInstance(String param1, String param2) {
        CountryOptionFragment fragment = new CountryOptionFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_country_option, container, false);

        LinearLayout addCountryLayout = (LinearLayout) view.findViewById(R.id.addCountry);
        addCountryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(CountryFragmentDirections.actionNavCountryToCountryFormInsertFragment());
                hideActionFragment();
            }
        });

        LinearLayout searchCountryLayout = (LinearLayout) view.findViewById(R.id.searchCountry);
        searchCountryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(CountryFragmentDirections.actionNavCountryToCountryFormSearchFragment());
                hideActionFragment();
            }
        });
        return view;
    }

    public void hideActionFragment(){
        this.dismiss();
    }

}
