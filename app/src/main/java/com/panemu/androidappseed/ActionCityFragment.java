package com.panemu.androidappseed;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;


public class ActionCityFragment extends BottomSheetFragment {

    private CityFormFragment cityFormFragment = new CityFormFragment();

    public ActionCityFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_acction_city, container, false);

        LinearLayout addCityLayout = (LinearLayout) view.findViewById(R.id.addCity);
        addCityLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(CityFragmenDirections.actionNavCityToCityFormInsertFragment());
                hideActionFragment();
            }
        });

        LinearLayout searchCity = (LinearLayout) view.findViewById(R.id.searchCity);
        searchCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(CityFragmenDirections.actionNavCityToCityFormSearcFragment());
                hideActionFragment();
            }
        });

        return view;
    }

    public void hideActionFragment(){
        this.dismiss();
    }

}
