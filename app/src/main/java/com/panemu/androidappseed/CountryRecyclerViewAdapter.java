package com.panemu.androidappseed;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.panemu.androidappseed.data.City;
import com.panemu.androidappseed.data.DtoCountry;
import com.panemu.androidappseed.misc.function.Consumer;

import java.util.List;

public class CountryRecyclerViewAdapter extends RecyclerView.Adapter<CountryRecyclerViewAdapter.ViewHolder> {

    private final List<DtoCountry> mValues;
    private final Consumer<DtoCountry> mListener;

    public CountryRecyclerViewAdapter(List<DtoCountry> items, Consumer<DtoCountry> listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_country, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final DtoCountry dtoCountry = mValues.get(position);
        holder.mItem = mValues.get(position);
        holder.mNameCountry.setText(dtoCountry.name);
        holder.mCapital.setText(dtoCountry.capital);
        holder.mContinen.setText(dtoCountry.continent);
        holder.mIndependence.setText(dtoCountry.independence);
        holder.mPopulations.setText(dtoCountry.population + "");

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.accept(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout viewBackground, viewForeground;
        public final View mView;
        public final TextView mNameCountry;
        public final TextView mCapital;
        public final TextView mContinen;
        public final TextView mIndependence;
        public final TextView mPopulations;
        public DtoCountry mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameCountry = (TextView) view.findViewById(R.id.nameCountry);
            mCapital = (TextView) view.findViewById(R.id.tvCapital);
            mContinen = (TextView) view.findViewById(R.id.tvContinent);
            mIndependence = (TextView) view.findViewById(R.id.tvIndependence);
            mPopulations = (TextView) view.findViewById(R.id.tvPopulate);
            viewBackground = view.findViewById(R.id.view_background_country);
            viewForeground = view.findViewById(R.id.view_foreground_country);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameCountry.getText() + "'";
        }
    }

    public void removeItem(int position) {
        mValues.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(DtoCountry item, int position) {
        mValues.add(position, item);
        notifyItemInserted(position);
    }
}
