package com.panemu.androidappseed;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.TextPaint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.plus.PlusOneButton;
import com.panemu.androidappseed.api.ApiCity;
import com.panemu.androidappseed.api.RetrofitClientInstance;
import com.panemu.androidappseed.data.City;
import com.panemu.androidappseed.data.DtoCountry;
import com.panemu.androidappseed.misc.PanemuUtil;
import com.panemu.androidappseed.misc.RestError;
import com.panemu.androidappseed.misc.TableData;
import com.panemu.androidappseed.ui.home.HomeFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


// disini bos item touchnya belum di deklarasikan
public class CityFormFragment extends Fragment implements AdapterView.OnItemSelectedListener{


    private City city;
    EditText name, versi;
    private Spinner continens, countrys;
    private Map<String,Integer> mapCountry = new HashMap<>();
    private ProgressDialog progress;

    public CityFormFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.city = (City) getArguments().getSerializable("city");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_city_form, container, false);

        continens = view.findViewById(R.id.spContinent);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(), R.array.spinner_continent, android.R.layout.simple_spinner_item);
        String select = city.continent;
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        continens.setAdapter(adapter);
        if (select != null){
            int spnPosition = adapter.getPosition(select);
            continens.setSelection(spnPosition);
        }
        continens.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                spinnerCountry();
                Object item = parent.getItemAtPosition(position);
                continens.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        countrys = view.findViewById(R.id.spCountryName);
        countrys.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                String selectCountrys = parent.getItemAtPosition(position).toString();
                countrys.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        name = view.findViewById(R.id.cityId);
//        countryDataId = view.findViewById(R.id.tvContryName);
        versi = view.findViewById(R.id.version);
        continens = view.findViewById(R.id.spContinent);

        name.setText(city.name);
        versi.setText(city.version.toString());

        this.city = (City) getArguments().getSerializable("city");

        Button btnSave = view.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(v -> {

            //membuat progress dialog
            progress = new ProgressDialog(this.getContext());
            progress.setCancelable(false);
            progress.setMessage("Loading ...");
            progress.setCancelable(true);
            progress.show();

            String nama = name.getText().toString();
            String country = countrys.getSelectedItem().toString();
            Integer countryId = mapCountry.get(country);
            String version = versi.getText().toString();
            ApiCity api = RetrofitClientInstance.getRetrofitInstance().create(ApiCity.class);
            Map<String, Object> payload = new HashMap<>();
            payload.put("name", nama);
            payload.put("countryId", countryId);
            payload.put("version", version);
            Call<City> call = api.updateCity(city.id, payload);
            call.enqueue(new Callback<City>() {
                @Override
                public void onResponse(Call<City> call, Response<City> response) {
                    Log.d("panemu", "call api");
                    if (response.isSuccessful()) {
                        PanemuUtil.showDialogInfo(getContext(), "Data Saved!");
                    } else {
                        RestError re = PanemuUtil.parseError(response);
                        Toast.makeText(CityFormFragment.this.getContext(), re.message, Toast.LENGTH_SHORT).show();
                        PanemuUtil.showDialogInfo(getContext(), re.message);
//                        Log.e("panemu", "after: " + re.toString());
                    }
                    progress.dismiss();
                }

                @Override
                public void onFailure(Call<City> call, Throwable t) {

                }
            });
        });

        Button btnDelete = view.findViewById(R.id.btnDeleteCity);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PanemuUtil.showDialogConfirmation(CityFormFragment.this.getContext(), "Delete confirmation", false, answer -> {
                    if ("YES_OK".equals(answer.name())) {
                        deletCity();
                    }
                });
            }
        });

        return view;
    }

    public void deletCity(){
        progress = new ProgressDialog(this.getContext());
        progress.setCancelable(false);
        progress.setMessage("Loading ...");
        progress.setCancelable(true);
        progress.show();
        ApiCity api = RetrofitClientInstance.getRetrofitInstance().create(ApiCity.class);
        Call<City> call = api.deleteCity(city.id);
        call.enqueue(new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                Log.d("panemu", "call api");
                if (response.isSuccessful()) {
                    PanemuUtil.showDialogInfo(getContext(), "Delete Success!");
                    NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                    navController.popBackStack();
                } else {
                    RestError re = PanemuUtil.parseError(response);
                    Toast.makeText(CityFormFragment.this.getContext(), re.message, Toast.LENGTH_SHORT).show();
                    PanemuUtil.showDialogInfo(getContext(), re.message);
//                        Log.e("panemu", "after: " + re.toString());
                }
                progress.dismiss();
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {

            }
        });
    }

    private void spinnerCountry(){
        String textContinen = continens.getSelectedItem().toString();
        ApiCity api = RetrofitClientInstance.getRetrofitInstance().create(ApiCity.class);
        Call<List<DtoCountry>> call = api.getCountryByContinent(textContinen);
        call.enqueue(new Callback<List<DtoCountry>>() {
            @Override
            public void onResponse(Call<List<DtoCountry>> call, Response<List<DtoCountry>> response) {
                List<DtoCountry> lstCountry = response.body();
                List<String> lstSpinner = new ArrayList<String>();
                mapCountry.clear();
                for (int i = 0; i < lstCountry.size(); i++){
                    lstSpinner.add(lstCountry.get(i).name);
                    mapCountry.put(lstCountry.get(i).name, lstCountry.get(i).id);
                }
                int selectedIdx = lstSpinner.indexOf(city.countryName);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(CityFormFragment.this.getContext(), android.R.layout.simple_spinner_item, lstSpinner);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                countrys.setAdapter(adapter);
                countrys.setSelection(selectedIdx);
                countrys.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {

                        Object item = parent.getItemAtPosition(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<DtoCountry>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        // Refresh the state of the +1 button each time the activity receives focus.
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
