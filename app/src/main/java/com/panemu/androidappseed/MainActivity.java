package com.panemu.androidappseed;

import android.content.Intent;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.panemu.androidappseed.data.AuthInfo;
import com.panemu.androidappseed.data.City;
import com.panemu.androidappseed.data.DtoUserListItem;
import com.panemu.androidappseed.misc.PanemuUtil;
import com.panemu.androidappseed.ui.gallery.GalleryViewModel;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;

import android.view.WindowManager;
import android.widget.ProgressBar;

import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private AuthInfo authInfo;
    TextView usrName;
    private static final String TAG = "";
    private AppBarConfiguration mAppBarConfiguration;
    private ProgressBar loadingProgressBar;
    private BottomSheetFragment bottomSheetFragment=new BottomSheetFragment();
//    private ActionCityFragment acCityFragment = new ActionCityFragment();
    private CountryOptionFragment countryOption = new CountryOptionFragment();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String sAuthInfo = sharedPref.getString("authInfo", null);
        Log.d(TAG,"User Login ==> " + sAuthInfo);


        usrName = findViewById(R.id.usrLogin);

        if (sAuthInfo != null && !sAuthInfo.trim().isEmpty()) {
            Gson gson = new Gson();
            AuthInfo authInfo = gson.fromJson(sAuthInfo, AuthInfo.class);
            PanemuUtil.setAuthInfo(authInfo);
        }
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loadingProgressBar = findViewById(R.id.loading);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//                logout();
                showBottomSheetDialogFragment();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        System.out.println("back from fragment");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    public void logout() {
        PanemuUtil.showDialogConfirmation(this, "Logout confirmation", false, answer -> {
            if ("YES_OK".equals(answer.name())) {

//                hideBottomSheetDialogFragment();
                loadingProgressBar.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = sharedPref.edit();
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                editor.clear();
                                editor.commit();
                                finish();
                                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                loadingProgressBar.setVisibility(View.GONE);
                            }
                        },
                        5000);
            } else {
                hideBottomSheetDialogFragment();
            }
        });
    }

    public void showBottomSheetDialogFragment() {
        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
    }

    public void hideBottomSheetDialogFragment() {
//        bottomSheetFragment.setPeekHeight(0);

        bottomSheetFragment.dismiss();
    }

    public void showCountryOption(){
        countryOption.show(getSupportFragmentManager(), countryOption.getTag());
    }
}
