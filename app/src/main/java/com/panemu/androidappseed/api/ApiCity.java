package com.panemu.androidappseed.api;

import com.panemu.androidappseed.data.City;
import com.panemu.androidappseed.data.DtoCountry;
import com.panemu.androidappseed.misc.TableData;
import com.panemu.androidappseed.misc.TableQuery;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiCity {

    @POST("city")
    Call<TableData<City>> getCityList(@Query("start") int start, @Query("max") int max, @Body TableQuery tq);

    @PUT("city/{id}")
    Call<City> updateCity( @Path("id") int id, @Body Map payload);

    @GET("city/countryList")
    Call<List<DtoCountry>> getCountryByContinent(@Query("continent") String continent);

    @PUT("city/{id}")
    Call<City> deleteCity(@Path("id") int id);

}

