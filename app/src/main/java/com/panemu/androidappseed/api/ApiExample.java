package com.panemu.androidappseed.api;

import com.panemu.androidappseed.data.City;
import com.panemu.androidappseed.misc.TableData;
import com.panemu.androidappseed.misc.TableQuery;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiExample {

    @GET("photos")
    Call<List<Map>> getExampleData();

    @POST("city")
    Call<TableData<City>> getCityList(@Query("start") int start, @Query("max") int max, @Body TableQuery tq);

}
