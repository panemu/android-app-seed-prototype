package com.panemu.androidappseed.api;

import com.panemu.androidappseed.data.DtoCountry;
import com.panemu.androidappseed.misc.TableData;
import com.panemu.androidappseed.misc.TableQuery;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiCountry {

    @POST("country")
    Call<TableData<DtoCountry>> getCountryList(@Query("start") int start, @Query("max") int max, @Body TableQuery tq);

    @PUT("country/{id}")
    Call<DtoCountry> saveCountry(@Path("id") int id, @Body Map payload);

    @DELETE("country/{id}")
    Call<DtoCountry> deleteCountry(@Path("id") int id);

}
