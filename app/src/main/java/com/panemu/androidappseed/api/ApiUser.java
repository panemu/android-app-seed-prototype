package com.panemu.androidappseed.api;

import com.panemu.androidappseed.data.AuthInfo;
import com.panemu.androidappseed.data.City;
import com.panemu.androidappseed.data.DtoUserListItem;
import com.panemu.androidappseed.misc.TableData;
import com.panemu.androidappseed.misc.TableQuery;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiUser {

    @POST("auth/authenticate")
    Call<AuthInfo> login(@Body Map<String, String> usernamePassword);

    @POST("user")
    Call<TableData<DtoUserListItem>> getUserList(@Query("start") int start, @Query("max") int max, @Body TableQuery tq);
}
