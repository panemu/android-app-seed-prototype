package com.panemu.androidappseed;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.panemu.androidappseed.api.ApiCity;
import com.panemu.androidappseed.api.RetrofitClientInstance;
import com.panemu.androidappseed.data.City;
import com.panemu.androidappseed.misc.PanemuUtil;
import com.panemu.androidappseed.misc.RestError;
import com.panemu.androidappseed.misc.TableData;
import com.panemu.androidappseed.misc.TableQuery;
import com.panemu.androidappseed.touchHelper.RecyclerItemTouchHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CityFragmen extends Fragment implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private RecyclerView recyclerView;
    private ProgressDialog progress;
    private ActionCityFragment acCityFragment = new ActionCityFragment();
    private List<City> cityList;
    private CityRecyclerViewAdapter adapter;
    private int startDataIndex = 0;
    private final int maxRecordPerFetch = 20;


    public static CityFragmen newInstance(int columnCount) {
        CityFragmen fragment = new CityFragmen();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        StaticObject.searchData = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d("panemu", getClass().getSimpleName() + " onCreateView");
        startDataIndex = 0;
        View view = inflater.inflate(R.layout.fragment_city_list, container, false);

        FloatingActionButton fab = (FloatingActionButton)getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("panemu", "show action");
                acCityFragment.show(getActivity().getSupportFragmentManager(), acCityFragment.getTag());
            }
        });

        recyclerView = view.findViewById(R.id.cityList);

        cityList = new ArrayList<>();
        adapter = new CityRecyclerViewAdapter(cityList, this::showDetail);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(CityFragmen.this.getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            boolean loading = true;

            int previousTotal = 0;
            int visibleThreshold = 10;
            int firstVisibleItem = 0;
            int visibleItemCount = 0;
            int totalItemCount = 0;

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }

                if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                    int initialSize = cityList.size();
                    startDataIndex = startDataIndex + maxRecordPerFetch;

                    loadData(() -> {
                        int updatedSize = cityList.size();
                        recyclerView.post (() -> {
                            adapter.notifyItemRangeInserted(initialSize, updatedSize);
                            Log.d("panemu", "inside runnable after load next page " + recyclerView.getAdapter().getItemCount());
                        });
                    });
//                    recyclerView.getAdapter().notifyDataSetChanged();
                    loading = true;

                }
            }
        });

        System.out.printf("back clicked");
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        loadData(() -> {
            Log.d("panemu", "callback reload");
            adapter = new CityRecyclerViewAdapter(cityList, CityFragmen.this::showDetail);
            recyclerView.setAdapter(adapter);
        });
        return view;
    }

    private void showDetail(City item) {
        Log.d("panemu", "show detail from parent fragment");
        CityFragmenDirections.ActionNavCityToCityFormFragment atc = CityFragmenDirections.actionNavCityToCityFormFragment(item);
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        navController.navigate(atc);

    }

    private void loadData(Runnable onFinishLoading) {
        Log.d("panemu", "loadData");
        progress = PanemuUtil.showProgressDialog(this.getContext());
        ApiCity api = RetrofitClientInstance.getRetrofitInstance().create(ApiCity.class);
        TableQuery tq = StaticObject.searchData;
        if (tq != null) {
            tq = StaticObject.searchData;
        } else {
            tq = new TableQuery();
        }

        //before network call
        Call<TableData<City>> callable = api.getCityList(startDataIndex, maxRecordPerFetch, tq);
        callable.enqueue(new Callback<TableData<City>>() {
            @Override
            public void onResponse(Call<TableData<City>> call, Response<TableData<City>> response) {
                //network call finish
                if (response.isSuccessful()) {
                    cityList.addAll(response.body().rows);
                    Log.d("panemu", "citylist size" + cityList.size());
                    progress.dismiss();
                    onFinishLoading.run();
                } else {
                    RestError re = PanemuUtil.parseError(response);
                    Toast.makeText(CityFragmen.this.getContext(), re.message, Toast.LENGTH_SHORT).show();
                    PanemuUtil.showDialogInfo(getContext(), re.message);
                    Log.e("panemu", "after: " + re.toString());
                }
            }

            @Override
            public void onFailure(Call<TableData<City>> call, Throwable t) {
                Log.e("panemu", t.getMessage());
                Toast.makeText(getContext(), "Error calling server222", Toast.LENGTH_SHORT).show();
            }
        });
        //after network call
    }


    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof CityRecyclerViewAdapter.ViewHolder) {
            // get the removed item name to display it in snack bar
            String name = cityList.get(viewHolder.getAdapterPosition()).name;

            // backup of removed item for undo purpose
            final City deletedItem = cityList.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
            adapter.removeItem(viewHolder.getAdapterPosition());

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(this.getView(), name + " removed from cart!", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // undo is selected, restore the deleted item
                    adapter.restoreItem(deletedItem, deletedIndex);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }

}
