package com.panemu.androidappseed;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.panemu.androidappseed.api.ApiCity;
import com.panemu.androidappseed.api.RetrofitClientInstance;
import com.panemu.androidappseed.data.City;
import com.panemu.androidappseed.data.DtoCountry;
import com.panemu.androidappseed.misc.PanemuUtil;
import com.panemu.androidappseed.misc.RestError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CityFormInsertFragment extends Fragment {

    private City city;
    EditText name, versi;
    private Spinner continens, countrys;
    private Map<String,Integer> mapCountry = new HashMap<>();
    private ProgressDialog progress;
    private ActionCityFragment fActionCity = new ActionCityFragment();

    public CityFormInsertFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static CityFormInsertFragment newInstance(String param1, String param2) {
        CityFormInsertFragment fragment = new CityFormInsertFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_city_form_insert, container, false);
//        fActionCity.hideActionFragment();
        continens = view.findViewById(R.id.spinContinent);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(), R.array.spinner_continent, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        continens.setAdapter(adapter);
        continens.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                spinnerCountry();
                Object item = parent.getItemAtPosition(position);
                continens.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        countrys = view.findViewById(R.id.spCountryName);
        countrys.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                String selectCountrys = parent.getItemAtPosition(position).toString();
                countrys.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        name = view.findViewById(R.id.etNameCity);
        versi = view.findViewById(R.id.version);
        continens = view.findViewById(R.id.spinContinent);
        this.city = (City) getArguments().getSerializable("city");

        Button btnSave = view.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(v -> {

            //membuat progress dialog
            progress = new ProgressDialog(this.getContext());
            progress.setCancelable(false);
            progress.setMessage("Loading ...");
            progress.setCancelable(true);
            progress.show();

            String nama = name.getText().toString();
            String country = countrys.getSelectedItem().toString();
            Integer countryId = mapCountry.get(country);
            String version = "0";
            ApiCity api = RetrofitClientInstance.getRetrofitInstance().create(ApiCity.class);
            Map<String, Object> payload = new HashMap<>();
            payload.put("name", nama);
            payload.put("countryId", countryId);
            payload.put("version", version);
            Call<City> call = api.updateCity(0, payload);
            call.enqueue(new Callback<City>() {
                @Override
                public void onResponse(Call<City> call, Response<City> response) {
                    Log.d("panemu", "call api");
                    if (response.isSuccessful()) {
                        PanemuUtil.showDialogInfo(getContext(), "Data Saved!");
                    } else {
                        RestError re = PanemuUtil.parseError(response);
                        Toast.makeText(CityFormInsertFragment.this.getContext(), re.message, Toast.LENGTH_SHORT).show();
                        PanemuUtil.showDialogInfo(getContext(), re.message);
//                        Log.e("panemu", "after: " + re.toString());
                    }
                    progress.dismiss();
                }

                @Override
                public void onFailure(Call<City> call, Throwable t) {

                }
            });
        });
        return view;

    }

    private void spinnerCountry(){
        String textContinen = continens.getSelectedItem().toString();
        ApiCity api = RetrofitClientInstance.getRetrofitInstance().create(ApiCity.class);
        Call<List<DtoCountry>> call = api.getCountryByContinent(textContinen);
        call.enqueue(new Callback<List<DtoCountry>>() {
            @Override
            public void onResponse(Call<List<DtoCountry>> call, Response<List<DtoCountry>> response) {
                List<DtoCountry> lstCountry = response.body();
                List<String> lstSpinner = new ArrayList<String>();
                mapCountry.clear();
                for (int i = 0; i < lstCountry.size(); i++){
                    lstSpinner.add(lstCountry.get(i).name);
                    mapCountry.put(lstCountry.get(i).name, lstCountry.get(i).id);
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(CityFormInsertFragment.this.getContext(), android.R.layout.simple_spinner_item, lstSpinner);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                countrys.setAdapter(adapter);
                countrys.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {

                        Object item = parent.getItemAtPosition(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<DtoCountry>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
