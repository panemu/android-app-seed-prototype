package com.panemu.androidappseed;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.panemu.androidappseed.api.ApiCountry;
import com.panemu.androidappseed.api.RetrofitClientInstance;
import com.panemu.androidappseed.data.DtoCountry;
import com.panemu.androidappseed.misc.PanemuUtil;
import com.panemu.androidappseed.misc.RestError;
import com.panemu.androidappseed.ui.JdatePicker.DatePickerFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CountryFormFragment extends Fragment {

    private final Calendar myCalendar = Calendar.getInstance();
    private DtoCountry dtoCountry;
    private EditText countryname, capital, independen, populas;
    private Spinner continen;
    private ProgressDialog progress;

    public CountryFormFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.dtoCountry = (DtoCountry) getArguments().getSerializable("DtoCountry");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_country_form, container, false);

        countryname = view.findViewById(R.id.etName);
        capital = view.findViewById(R.id.etCapital);
        continen = view.findViewById(R.id.etContinent);
        independen = view.findViewById(R.id.etIndependence);
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        independen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerFragment dp = new DatePickerFragment(independen);
                dp.show(getActivity().getSupportFragmentManager(), dp.getTag());

            }
        });

        populas = view.findViewById(R.id.etPopulation);

        countryname.setText(dtoCountry.name);
        capital.setText(dtoCountry.capital);
        independen.setText(dtoCountry.independence);
        populas.setText(dtoCountry.population.toString());
        continen = view.findViewById(R.id.etContinent);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(), R.array.spinner_continent, android.R.layout.simple_spinner_item);
        String select = dtoCountry.continent;
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        continen.setAdapter(adapter);
        if (select != null){
            int spnPosition = adapter.getPosition(select);
            continen.setSelection(spnPosition);
        }
        continen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                Object item = parent.getItemAtPosition(position);
                continen.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Button button = view.findViewById(R.id.btnSaveCountry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress = new ProgressDialog(CountryFormFragment.this.getContext());
                progress.setCancelable(false);
                progress.setMessage("Loading ...");
                progress.setCancelable(true);
                progress.show();

                String countryName = countryname.getText().toString();
                String capitalName = capital.getText().toString();
                String continenName = continen.getSelectedItem().toString();
                String independens = independen.getText().toString();
                String populat = populas.getText().toString();
                ApiCountry api = RetrofitClientInstance.getRetrofitInstance().create(ApiCountry.class);
                Map<String, Object> payload = new HashMap<>();
                payload.put("name", countryName);
                payload.put("continent", continenName);
                payload.put("capital", capitalName);
                payload.put("population", populat);
                payload.put("independence", independens);
                Call<DtoCountry> call = api.saveCountry(dtoCountry.id, payload);
                call.enqueue(new Callback<DtoCountry>() {
                    @Override
                    public void onResponse(Call<DtoCountry> call, Response<DtoCountry> response) {
                        if (response.isSuccessful()) {
                            PanemuUtil.showDialogInfo(getContext(), "Data Saved!");
                        } else {
                            RestError re = PanemuUtil.parseError(response);
                            Toast.makeText(CountryFormFragment.this.getContext(), re.message, Toast.LENGTH_SHORT).show();
                            PanemuUtil.showDialogInfo(getContext(), re.message);
                            Log.e("panemu", "after: " + re.toString());
                        }
                        progress.dismiss();
                    }

                    @Override
                    public void onFailure(Call<DtoCountry> call, Throwable t) {

                    }
                });

            }
        });

        Button btnDelete = view.findViewById(R.id.btn_deleteCountry);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PanemuUtil.showDialogConfirmation(CountryFormFragment.this.getContext(), "Delete confirmation", false, answer -> {
                    if ("YES_OK".equals(answer.name())) {
                        delete();
                    }
                });
            }
        });

        return view;
    }

    public void delete(){
        progress = new ProgressDialog(CountryFormFragment.this.getContext());
        progress.setCancelable(false);
        progress.setMessage("Loading ...");
        progress.setCancelable(true);
        progress.show();

        ApiCountry api = RetrofitClientInstance.getRetrofitInstance().create(ApiCountry.class);
        Call<DtoCountry> call = api.deleteCountry(dtoCountry.id);
        call.enqueue(new Callback<DtoCountry>() {
            @Override
            public void onResponse(Call<DtoCountry> call, Response<DtoCountry> response) {
                if (response.isSuccessful()) {
                    PanemuUtil.showDialogInfo(getContext(), "Deleted Succes!");
                    NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                    navController.popBackStack();
                } else {
                    RestError re = PanemuUtil.parseError(response);
                    Toast.makeText(CountryFormFragment.this.getContext(), re.message, Toast.LENGTH_SHORT).show();
                    PanemuUtil.showDialogInfo(getContext(), re.message);
                    Log.e("panemu", "after: " + re.toString());
                }
                progress.dismiss();
            }

            @Override
            public void onFailure(Call<DtoCountry> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
