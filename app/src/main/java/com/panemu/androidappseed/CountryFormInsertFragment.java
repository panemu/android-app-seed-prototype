package com.panemu.androidappseed;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.panemu.androidappseed.api.ApiCountry;
import com.panemu.androidappseed.api.RetrofitClientInstance;
import com.panemu.androidappseed.data.DtoCountry;
import com.panemu.androidappseed.misc.PanemuUtil;
import com.panemu.androidappseed.misc.RestError;
import com.panemu.androidappseed.ui.JdatePicker.DatePickerFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CountryFormInsertFragment extends Fragment {

    EditText name, capital, independence, population;
    Spinner continent;
    ProgressDialog progress;
    DtoCountry dtoCountry;

    public CountryFormInsertFragment() {
        // Required empty public constructor
    }

    public static CountryFormInsertFragment newInstance(String param1, String param2) {
        CountryFormInsertFragment fragment = new CountryFormInsertFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_country_form_insert, container, false);

        name = view.findViewById(R.id.edt_countryName);
        capital = view.findViewById(R.id.edt_capital);
        independence = view.findViewById(R.id.edt_independence);
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        independence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerFragment dp = new DatePickerFragment(independence);
                dp.show(getActivity().getSupportFragmentManager(), dp.getTag());

            }
        });
        population = view.findViewById(R.id.edt_population);
        continent = view.findViewById(R.id.spnr_continen);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(), R.array.spinner_continent, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        continent.setAdapter(adapter);
        continent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                Object item = parent.getItemAtPosition(position);
                continent.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Button btnsave = view.findViewById(R.id.btn_saveCountry);
        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress = new ProgressDialog(CountryFormInsertFragment.this.getContext());
                progress.setCancelable(false);
                progress.setMessage("Loading ...");
                progress.setCancelable(true);
                progress.show();

                String countryName = name.getText().toString();
                String capitalName = capital.getText().toString();
                String continenName = continent.getSelectedItem().toString();
                String independens = independence.getText().toString();
                String populat = population.getText().toString();
                ApiCountry api = RetrofitClientInstance.getRetrofitInstance().create(ApiCountry.class);
                Map<String, Object> payload = new HashMap<>();
                payload.put("name", countryName);
                payload.put("continent", continenName);
                payload.put("capital", capitalName);
                payload.put("population", populat);
                payload.put("independence", independens);
                Call<DtoCountry> call = api.saveCountry(0, payload);
                call.enqueue(new Callback<DtoCountry>() {
                    @Override
                    public void onResponse(Call<DtoCountry> call, Response<DtoCountry> response) {
                        if (response.isSuccessful()) {
                            PanemuUtil.showDialogInfo(getContext(), "Data Saved!");
                        } else {
                            RestError re = PanemuUtil.parseError(response);
                            Toast.makeText(CountryFormInsertFragment.this.getContext(), re.message, Toast.LENGTH_SHORT).show();
                            PanemuUtil.showDialogInfo(getContext(), re.message);
                            Log.e("panemu", "after: " + re.toString());
                        }
                        progress.dismiss();
                    }

                    @Override
                    public void onFailure(Call<DtoCountry> call, Throwable t) {

                    }
                });
            }
        });

        return view;
    }

}
