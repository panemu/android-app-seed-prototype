package com.panemu.androidappseed;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.panemu.androidappseed.data.City;
import com.panemu.androidappseed.misc.function.Consumer;

import java.util.List;


public class CityRecyclerViewAdapter extends RecyclerView.Adapter<CityRecyclerViewAdapter.ViewHolder> {

    private final List<City> mValues;
    private final Consumer<City> mListener;

    public CityRecyclerViewAdapter(List<City> items, Consumer<City> listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_city, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final City city = mValues.get(position);
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(city.id + "" );
        holder.mNameView.setText(city.name);
        holder.mCountryNameView.setText(city.countryName);
        holder.mVersion.setText(city.version.toString());
        holder.mContinent.setText(city.continent);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.accept(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout viewBackground, viewForeground;
        public City mItem;
        public final View mView;
        public final TextView mIdView;
        public final TextView mNameView;
        public final TextView mCountryNameView;
        public final TextView mContinent;
        public final TextView mVersion;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_id);
            mNameView = (TextView) view.findViewById(R.id.name);
            mCountryNameView = (TextView) view.findViewById(R.id.countryName);
            mVersion = (TextView) view.findViewById(R.id.versi);
            mContinent = (TextView) view.findViewById(R.id.tvContinen);
            viewBackground = view.findViewById(R.id.view_background);
            viewForeground = view.findViewById(R.id.view_foreground);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }

    public void removeItem(int position) {
        mValues.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(City item, int position) {
        mValues.add(position, item);
        notifyItemInserted(position);
    }
}
