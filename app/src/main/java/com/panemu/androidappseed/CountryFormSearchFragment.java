package com.panemu.androidappseed;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.panemu.androidappseed.data.DtoCountry;
import com.panemu.androidappseed.misc.TableCriteria;
import com.panemu.androidappseed.misc.TableQuery;
import com.panemu.androidappseed.ui.JdatePicker.DatePickerFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;


public class CountryFormSearchFragment extends Fragment {

    DtoCountry dtoCountry;
    ProgressDialog progress;

    public CountryFormSearchFragment() {
        // Required empty public constructor
    }

    public static CountryFormSearchFragment newInstance(String param1, String param2) {
        CountryFormSearchFragment fragment = new CountryFormSearchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_country_form_search, container, false);
        final EditText name, capital, independence, population, continent;

        name = view.findViewById(R.id.eds_name);
        capital = view.findViewById(R.id.eds_capital);
        population = view.findViewById(R.id.eds_population);
        independence = view.findViewById(R.id.eds_independence);
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        independence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerFragment dp = new DatePickerFragment(independence);
                dp.show(getActivity().getSupportFragmentManager(), dp.getTag());

            }
        });

        continent = view.findViewById(R.id.eds_continent);

        Button btnSearch = view.findViewById(R.id.btn_searchCountry);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String countryName = (String) name.getText().toString();
                TableQuery tq = new TableQuery();
                if (countryName != null && !countryName.trim().isEmpty()) {
                    TableCriteria tc = new TableCriteria();
                    tc.attributeName = "name";
                    tc.value = countryName.trim();
                    tq.tableCriteria.add(tc);
                }

                String country = (String) continent.getText().toString();
                if (country != null && !country.trim().isEmpty()) {
                    TableCriteria tc = new TableCriteria();
                    tc.attributeName = "continent";
                    tc.value = country.trim();
                    tq.tableCriteria.add(tc);
                }

                String capitalCity = (String) capital.getText().toString();
                if (capitalCity != null && !capitalCity.trim().isEmpty()) {
                    TableCriteria tc = new TableCriteria();
                    tc.attributeName = "capital";
                    tc.value = capitalCity.trim();
                    tq.tableCriteria.add(tc);
                }

                String independenc = (String) independence.getText().toString();
                if (independenc != null && !independenc.trim().isEmpty()) {
                    TableCriteria tc = new TableCriteria();
                    tc.attributeName = "independence";
                    tc.value = independenc.trim();
                    tq.tableCriteria.add(tc);
                }

                String populat = (String) population.getText().toString();
                if (populat != null && !populat.trim().isEmpty()) {
                    TableCriteria tc = new TableCriteria();
                    tc.attributeName = "population";
                    tc.value = populat.trim();
                    tq.tableCriteria.add(tc);
                }

                StaticObject.searchData = tq;
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.popBackStack();
            }
        });

        return view;
    }

}
