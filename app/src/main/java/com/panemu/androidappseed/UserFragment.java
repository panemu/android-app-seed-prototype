package com.panemu.androidappseed;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.panemu.androidappseed.api.ApiExample;
import com.panemu.androidappseed.api.ApiUser;
import com.panemu.androidappseed.api.RetrofitClientInstance;
import com.panemu.androidappseed.data.DtoUserListItem;
import com.panemu.androidappseed.misc.PanemuUtil;
import com.panemu.androidappseed.misc.RestError;
import com.panemu.androidappseed.misc.TableData;
import com.panemu.androidappseed.misc.TableQuery;
import com.panemu.androidappseed.ui.home.HomeFragment;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private RecyclerView recyclerView;
    private ProgressDialog progress;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public UserFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static UserFragment newInstance(int columnCount) {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new UserRecyclerViewAdapter(new ArrayList<>(), this::showDetail));
        }
        reloadData();
        return view;
    }

    private void showDetail(DtoUserListItem item) {
        Log.d("panemu", "show detail from parent fragment");
        UserFragmentDirections.ActionNavUsersToUserFormFragment act = UserFragmentDirections.actionNavUsersToUserFormFragment(item);

        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        navController.navigate(act);
    }

    private void reloadData() {
        progress = new ProgressDialog(this.getContext());
        progress.setCancelable(false);
        progress.setMessage("Mohon Bersabar ...");
        progress.setCancelable(true);
        progress.show();
        ApiUser api = RetrofitClientInstance.getRetrofitInstance().create(ApiUser.class);
        Call<TableData<DtoUserListItem>> callable = api.getUserList(0, 10, new TableQuery());
        callable.enqueue(new Callback<TableData<DtoUserListItem>>() {
            @Override
            public void onResponse(Call<TableData<DtoUserListItem>> call, Response<TableData<DtoUserListItem>> response) {
                if (response.isSuccessful()) {
                    UserRecyclerViewAdapter adapter = new UserRecyclerViewAdapter(response.body().rows, UserFragment.this::showDetail);
                    recyclerView.setAdapter(adapter);
                } else {
                    RestError re = PanemuUtil.parseError(response);
                    Toast.makeText(UserFragment.this.getContext(), re.message, Toast.LENGTH_SHORT).show();
                    PanemuUtil.showDialogInfo(getContext(), re.message);
                    Log.e("panemu", "after: " + re.toString());
                }
                progress.dismiss();
            }

            @Override
            public void onFailure(Call<TableData<DtoUserListItem>> call, Throwable t) {
                Log.e("panemu", t.getMessage());
                Toast.makeText(getContext(), "Error calling server222", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
