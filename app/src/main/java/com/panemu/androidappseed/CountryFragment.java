package com.panemu.androidappseed;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.panemu.androidappseed.api.ApiCountry;
import com.panemu.androidappseed.api.RetrofitClientInstance;
import com.panemu.androidappseed.data.DtoCountry;
import com.panemu.androidappseed.misc.PanemuUtil;
import com.panemu.androidappseed.misc.RestError;
import com.panemu.androidappseed.misc.TableData;
import com.panemu.androidappseed.misc.TableQuery;
import com.panemu.androidappseed.touchHelper.RecyclerItemTouchHelperCountry;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CountryFragment extends Fragment implements RecyclerItemTouchHelperCountry.RecyclerItemTouchHelperCountryListener{

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private RecyclerView recyclerView;
    private ProgressDialog progress;
    private List<DtoCountry> countryList;
    private CountryRecyclerViewAdapter adapter;
    private int startDataIndex = 0;
    private final int maxRecordPerFetch = 20;

    public CountryFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static CountryFragment newInstance(int columnCount) {
        CountryFragment fragment = new CountryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        StaticObject.searchData = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_country_list, container, false);

        FloatingActionButton fab = (FloatingActionButton)getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).showCountryOption();
            }
        });

        recyclerView = view.findViewById(R.id.country_list);

        countryList = new ArrayList<>();
        adapter = new CountryRecyclerViewAdapter(countryList, this::showDetail);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(CountryFragment.this.getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            boolean loading = true;

            int previousTotal = 0;
            int visibleThreshold = 10;
            int firstVisibleItem = 0;
            int visibleItemCount = 0;
            int totalItemCount = 0;

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }

                if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                    int initialSize = countryList.size();
                    startDataIndex = startDataIndex + maxRecordPerFetch;

                    reloadData(() -> {
                        int updatedSize = countryList.size();
                        recyclerView.post (() -> {
                            adapter.notifyItemRangeInserted(initialSize, updatedSize);
                            Log.d("panemu", "inside runnable after load next page " + recyclerView.getAdapter().getItemCount());
                        });
                    });
//                    recyclerView.getAdapter().notifyDataSetChanged();
                    loading = true;

                }
            }
        });

        System.out.printf("back clicked");
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelperCountry(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        reloadData(() -> {
            Log.d("panemu", "callback reload");
            adapter = new CountryRecyclerViewAdapter(countryList, CountryFragment.this::showDetail);
            recyclerView.setAdapter(adapter);
        });
        return view;
    }

    private void showDetail(DtoCountry item) {
        Log.d("panemu", "show detail from parent fragment");
        CountryFragmentDirections.ActionNavCountryToCountryFormFragment atc = CountryFragmentDirections.actionNavCountryToCountryFormFragment(item);
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        navController.navigate(atc);
    }

    private void reloadData(Runnable onFinishLoading) {
        progress = PanemuUtil.showProgressDialog(this.getContext());
        ApiCountry api = RetrofitClientInstance.getRetrofitInstance().create(ApiCountry.class);
        TableQuery tq = StaticObject.searchData;
        if (tq != null) {
            tq = StaticObject.searchData;
        } else {
            tq = new TableQuery();
        }
        Call<TableData<DtoCountry>> callable = api.getCountryList(startDataIndex, maxRecordPerFetch, tq);
        callable.enqueue(new Callback<TableData<DtoCountry>>() {
            @Override
            public void onResponse(Call<TableData<DtoCountry>> call, Response<TableData<DtoCountry>> response) {
                //network call finish
                if (response.isSuccessful()) {
                    countryList.addAll(response.body().rows);
                    Log.d("panemu", "country size" + countryList.size());
                    progress.dismiss();
                    onFinishLoading.run();
                } else {
                    RestError re = PanemuUtil.parseError(response);
                    Toast.makeText(CountryFragment.this.getContext(), re.message, Toast.LENGTH_SHORT).show();
                    PanemuUtil.showDialogInfo(getContext(), re.message);
                    Log.e("panemu", "after: " + re.toString());
                }
            }

            @Override
            public void onFailure(Call<TableData<DtoCountry>> call, Throwable t) {
                Log.e("panemu", t.getMessage());
                Toast.makeText(getContext(), "Error calling server222", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof CountryRecyclerViewAdapter.ViewHolder) {
            // get the removed item name to display it in snack bar
            String name = countryList.get(viewHolder.getAdapterPosition()).name;

            // backup of removed item for undo purpose
            final DtoCountry deletedItem = countryList.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
            adapter.removeItem(viewHolder.getAdapterPosition());

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(this.getView(), name + " removed from cart!", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // undo is selected, restore the deleted item
                    adapter.restoreItem(deletedItem, deletedIndex);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }
}
