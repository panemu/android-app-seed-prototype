package com.panemu.androidappseed.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.panemu.androidappseed.BottomSheetFragment;
import com.panemu.androidappseed.MainActivity;
import com.panemu.androidappseed.R;
import com.panemu.androidappseed.api.ApiExample;
import com.panemu.androidappseed.api.RetrofitClientInstance;
import com.panemu.androidappseed.data.City;
import com.panemu.androidappseed.misc.PanemuUtil;
import com.panemu.androidappseed.misc.RestError;
import com.panemu.androidappseed.misc.TableData;
import com.panemu.androidappseed.misc.TableQuery;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private BottomSheetFragment bottomSheetFragment =new BottomSheetFragment();;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
//        final TextView textView = root.findViewById(R.id.text_home);
       /* homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
*/
        Button btnApiTest = root.findViewById(R.id.btnApiTest);
        btnApiTest.setOnClickListener(v -> {
            ApiExample api = RetrofitClientInstance.getRetrofitInstance().create(ApiExample.class);
            Call<TableData<City>> call = api.getCityList(0,2, new TableQuery());
            call.enqueue(new Callback<TableData<City>>() {
                @Override
                public void onResponse(Call<TableData<City>> call, Response<TableData<City>> response) {
                    Log.d("panemu", "call api");
                    if (response.isSuccessful()) {
                        TableData<City> td = response.body();
                        Log.d("panemu", "totalRows: " + td.totalRows);
                        for (City c :
                                td.rows) {
                            Log.d("panemu","city name: " + c.name);
                        }
                    } else {
                        RestError re = PanemuUtil.parseError(response);
                        Toast.makeText(HomeFragment.this.getContext(), re.message, Toast.LENGTH_SHORT).show();
                        PanemuUtil.showDialogInfo(getContext(), re.message);
                        Log.e("panemu", "after: " + re.toString());
                    }
                }

                @Override
                public void onFailure(Call<TableData<City>> call, Throwable t) {
                    Log.e("panemu", t.getMessage());
                    Toast.makeText(getContext(), "Error calling server222", Toast.LENGTH_SHORT).show();
                }
            });
        });

        Button btnDialogInfo = root.findViewById(R.id.btnDialogInfo);
        Button btnDialogError = root.findViewById(R.id.btnDialogError);
        Button btnDialogConfirm = root.findViewById(R.id.btnDialogConfirm);
        Button btnDialogConfirm2 = root.findViewById(R.id.btnDialogConfirm2);
        Button btnCustom1 = root.findViewById(R.id.btnCustom1);
        Button btnCustom2 = root.findViewById(R.id.btnCustom2);

        btnDialogInfo.setOnClickListener(v -> {
            PanemuUtil.showDialogInfo(getContext(), "Information message example");
        });

        btnDialogError.setOnClickListener(v -> {
            PanemuUtil.showDialogError(getContext(), "Error message example");
        });

        btnDialogConfirm.setOnClickListener(v -> {
            PanemuUtil.showDialogConfirmation(getContext(), "Please select yes or no", false, answer -> {
                PanemuUtil.showDialogInfo(getContext(), "Your answer is " + answer);
            });
        });

        btnDialogConfirm2.setOnClickListener(v -> {
            PanemuUtil.showDialogConfirmation(getContext(), "Please select yes, no or cancel", true, answer -> {
                PanemuUtil.showDialogInfo(getContext(), "Your answer is " + answer);
            });
        });

        btnCustom1.setOnClickListener(v -> {
            PanemuUtil.showMessageDialog(getContext(),
                    PanemuUtil.MessageType.INFO,
                    "Custom Title",
                    "Message: lorem ipsum dolor sit amet, this message is rather long",
                    "Continue",
                    null,
                    null,
                    null);
        });

        btnCustom2.setOnClickListener(v -> {
            PanemuUtil.showMessageDialog(getContext(),
                    PanemuUtil.MessageType.INFO,
                    "3 Buttons Title",
                    "The button labels in this message are custom.",
                    "Execute",
                    "Ignore",
                    "Remind me later",
                    answer -> {
                        PanemuUtil.showDialogInfo(getContext(), "Your answer is " + answer);
                    });
        });

        FloatingActionButton fab = (FloatingActionButton)getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).showBottomSheetDialogFragment();
            }
        });

        return root;
    }

}