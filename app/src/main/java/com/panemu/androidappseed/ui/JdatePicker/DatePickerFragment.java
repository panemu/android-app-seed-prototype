package com.panemu.androidappseed.ui.JdatePicker;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import com.panemu.androidappseed.CountryFormFragment;
import com.panemu.androidappseed.misc.function.Consumer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    final Calendar c = Calendar.getInstance();
    private Consumer<String> onDateSelected;
    private EditText txtDate;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public DatePickerFragment(Consumer<String> onDateSelected) {
        this.onDateSelected = onDateSelected;
    }

    public DatePickerFragment(EditText txtDate) {
        this.txtDate = txtDate;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Gunakan waktu sekarang sebagai waktu default


        if (txtDate != null && !txtDate.getText().toString().trim().isEmpty() ) {
            try {
                Date selectedDate = dateFormat.parse(txtDate.getText().toString().trim());
                c.setTime(selectedDate);
            } catch (ParseException e) {
                Log.e("panemu", "Error parsing " + txtDate.getText().toString().trim());
            }
        } else {
            c.setTime(new Date());
        }

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), DatePickerFragment.this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        String selectedDate = dateFormat.format(c.getTime());
        if (txtDate != null) {
            txtDate.setText(selectedDate);
        } else {
            onDateSelected.accept(selectedDate);
        }
        Log.d("panemu", "onDateSet: " + selectedDate);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.txtDate = null;
    }
}
